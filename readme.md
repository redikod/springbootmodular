
Create Question JSON:

url: localhost:8080/questions

`
{
	"title":"This is my title",
	"description":"This is my description"
}
`

2. Get paginated Questions GET /questions?page=0&size=2&sort=createdAt,desc

Url: localhost:8080/questions?page=0?size=2?sort=createdAt,desc


3. Create Answer POST /questions/{questionId}/answers

URL: localhost:8080/questions/1000/answers/
`
{
	"text":"its very easy answers one"
}
`

4. Get all answers of a Question GET /questions/{questionId}/answers
URL: localhost:8080/questions/1000/answers/

Ref:
https://github.com/callicoder/spring-boot-postgresql-jpa-hibernate-rest-api-demo
