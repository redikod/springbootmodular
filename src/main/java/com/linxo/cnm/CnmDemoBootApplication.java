package com.linxo.cnm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class CnmDemoBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(CnmDemoBootApplication.class, args);
	}

}

